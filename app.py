from flask import Flask, redirect, url_for, request,jsonify
import subprocess
import json
import threading
import requests
import sys
import traceback
import logging
import os
from datetime import datetime
from snet import sdk
import time
from config import config, rpc_endpoints
import ast
from utils import *




proxy_port=6000
config['eth_rpc_endpoint']=rpc_endpoints[0]

except:
    deployment_mode=''

app = Flask(__name__)
logging.basicConfig()
app.logger.info(str(rpc_endpoints))


@app.route('/sdk_call',methods=['POST','GET'])
def sdkCall():
    declarations=request.declarations
    service_name=request.service_name

    proto_defnition=declarations["protobuf_definition"]
    function_name=declarations["function"]
    service_input=declarations["input"]
    service_stub=declarations["service_stub"]
    headline=headline
    proto_file_name="proto_"+service_name+".proto"
    f = open(proto_file_name, "w")
    f.write(proto_defnition)
    f.close()

    subprocess.call(["python3", "-m", "grpc_tools.protoc",
            "-I.", "--python_out=.", "--grpc_python_out=." ,
            proto_file_name])

    service_name=service_name.replace("-","_")
    proto_service_pb2="proto_"+service_name+"_pb2"
    proto_service_pb2_grpc="proto_"+service_name+"_pb2_grpc"
    proto_pb2 = __import__(proto_service_pb2)
    proto_pb2_grpc = __import__(proto_service_pb2_grpc)

    stub = getattr(proto_pb2_grpc, service_stub)(channel)
    req = getattr(proto_pb2, service_input)(headline=headline,body=body)
    proc=function_name
    response=utils.call_sdk(req,proc,stub,org_id,service_id)
    return response

@app.before_first_request
def set_account():
    subprocess.run([ "snet","identity","create","snet-aigent","key","--private-key",
                    config['private_key']])
    subprocess.run(["snet","network","ropsten"])
    subprocess.run(["snet","identity","snet-aigent"])

utils=utils(service,rpc_endpoints,deployment_mode,proxy_port)

if __name__ == '__main__':
    app.run(host ='0.0.0.0',port = proxy_port,debug = False)

